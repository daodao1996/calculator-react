import React from 'react';
import './methodButton.css';

const MethodButton = (props) => {
  return (
    <button onClick={props.handleClick}
            name={props.textValue}>{props.textValue}</button>
  );
};

export default MethodButton;