import React from 'react';
import './operationInput.css';

class OperationalInput extends React.Component{
  constructor(props){
    super(props);
  }

  render() {
    return (
      <input type="text"
             className={this.props.classname}
             value={this.props.result}
             onChange={this.props.handleChange}/>
    );
  }
}

export default OperationalInput;