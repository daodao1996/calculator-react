import React from 'react';
import './calculator.css';
import MethodButton from "./MethodButton";
import OperationalInput from './OperationalInput';

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      calculateResult: '',
      firstNumber: '',
      secondNumber: '',
      operate: ''
    };

    this.operations = {
      '+': (a, b) => a + b,
      '-': (a, b) => a - b,
      'X': (a, b) => a * b,
      '/': (a, b) => a / b
    };

    this.handleFirstNumberChange = this.handleFirstNumberChange.bind(this);
    this.handleSecondNumberChange = this.handleSecondNumberChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAC = this.handleAC.bind(this);
    this.handleOperate = this.handleOperate.bind(this);
    this.handlePercent = this.handlePercent.bind(this);
  }


  handleFirstNumberChange(ele) {
    this.setState({firstNumber: ele.target.value});
  }

  handleSecondNumberChange(ele) {
    this.setState({secondNumber: ele.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    const result = this.operations[this.state.operate](parseFloat(this.state.firstNumber), parseFloat(this.state.secondNumber)).toFixed(2);
    this.setState({calculateResult: result});
  }

  handleOperate(ele) {
    this.setState({operate: ele.target.name})
  }

  handlePercent(ele) {
    this.setState({operate: ele.target.name});
    if (this.state.firstNumber !== "" && parseFloat(this.state.firstNumber)) {
      this.setState({calculateResult: parseFloat(this.state.firstNumber) / 100});
    } else if (this.state.secondNumber !== "" && parseFloat(this.state.secondNumber)) {
      this.setState({calculateResult: parseFloat(this.state.secondNumber) / 100});
    }
  }

  handleAC() {
    this.setState(
      {
        calculateResult: '',
        firstNumber: '',
        secondNumber: ''
      }
    );
  }

  render() {
    return (
      <main className="App">
        <div className={'buttons'}>
          <MethodButton textValue={'AC'} handleClick={this.handleAC}/>
          <MethodButton textValue={'%'} handleClick={this.handlePercent}/>
          <MethodButton textValue={'+'} handleClick={this.handleOperate}/>
          <MethodButton textValue={'-'} handleClick={this.handleOperate}/>
          <MethodButton textValue={'X'} handleClick={this.handleOperate}/>
          <MethodButton textValue={'/'} handleClick={this.handleOperate}/>
        </div>
        <div className={'inputs'}>
          <OperationalInput classname={'result'} result={this.state.calculateResult}/>
          <form action="" onSubmit={this.handleSubmit}>
            <OperationalInput classname={'firstNumber'}
                              handleChange={this.handleFirstNumberChange}
                              result={this.state.firstNumber}/>
            <OperationalInput classname={'secondNumber'}
                              handleChange={this.handleSecondNumberChange}
                              result={this.state.secondNumber}/>
            <input className={'submit'} type="submit" value={'='}/>
          </form>
        </div>
      </main>
    );
  }
}

export default Calculator;
